# Trip Cost Calculator App

This is a simple mobile app created on Flutter.

The app need to get the next info:
- Distance trip
- Performance per unit
- Fuel price

Distance can be on km or mi.

# ScreenShoots

<p align="center">
  <img src="/images/1.png" width="200" alt="1"/>
  <img src="/images/2.png" width="200" alt="2"/> 
  <img src="/images/3.png" width="200" alt="3"/>
</p>

**-Screenshoot get on Android One Pie, API level 29.**
 

# Authors
* **José Martínez** - [JMartinezCL](https://gitlab.com/JMartinezCL)

